# ChungCuTSGLotusSaiDongLongBien


<div class="project-module js-project-module--text module project-module-text text align-center module-content-text">
<div class="main-text">
<div>Tên thương mại dự án : <a href="http://kenhbatdongsanviet.com/tsg-lotus-sai-dong">TSG Lotus Sài Đồng</a></div>
<div>Chủ đầu tư: Tập Đoàn TSG Việt Nam
Tọa lạc tại : số190 Phố Sài Đồng, phường Việt Hưng, quận Long Biên, Tp.HàNội
Tên dự án : Khu nhà ở cao tầng kết hợp dịch vụ thương mại nhàở thấp tầng vànhà trẻ mẫu giáo
Tổng diện tích:10.015 m2
Diện tích xây dựng :2.021 m2
Mật độ xây dựng:40%
Chiều cao công trình :84,55m</div>
</div>
</div>
<div class="spacer"></div>
<div class="project-module grid--main js-grid-main  grid--ready">
<div class="grid__item-container js-project-lightbox-link js-grid-item-container lightbox-link hover-icon-enabled" data-height="430" data-width="640" data-flex-grow="386.9767441860465"><img class="grid__item-image js-grid__item-image grid__item-image-lazy js-lazy image-loaded" src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/ca156874797865.5c3aad6f83dea.jpg" sizes="831px" srcset="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ca156874797865.5c3aad6f83dea.jpg 600w,https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/ca156874797865.5c3aad6f83dea.jpg 640w," /></div>
</div>
<div class="spacer"></div>
<div class="project-module js-project-module--text module project-module-text text align-center module-content-text">
<div class="main-text">
<div>_LOẠI HÌNH PHÁT TRIỂN_</div>
<div>☑KHU NHÀỞ THẤP TẦNG (SHOPHOUSE 2 TRONG 1)</div>
<div>Diện tích đất xây dựng:1.710 m2</div>
<div>Diện tích xây dựng:1.365 m2</div>
<div>Mật độ xây dựng:79,8%</div>
<div>Tổng diện tích sàn:6.825 m2</div>
<div>Tầng cao:05 tầng</div>
<div>Shophouse trung bình từ105-145 triệu/m2; Kiot từ 26-45 triệu/m2</div>
<div>Thời gian bàn giao: Quý 3/2019</div>
<div>Đơn vị thi công : TSG Việt Nam Holdings</div>
<div>Đơn vị thiết kế: Công ty cổ phần xây dựng RECO</div>
<div>Phụ trách tư vấn kinh doanh: TSG LAND</div>
<div>☑KHU NHÀ TRẺ:</div>
<div>Diện tích đất xây dựng:1.280 m2</div>
<div>Diện tích xây dựng:512 m2</div>
<div>Mật độ xây dựng:40%</div>
<div>Tổng diện tích sàn:1.536 m2</div>
<div>Tầng cao: 3 tầng</div>
<div>☑KHU CĂN HỘ CAO TẦNG : <a href="http://kenhbatdongsanviet.com/tsg-lotus-sai-dong">Chung cư Tsg Lotus Sài Đồng</a></div>
<div>Diện tích đất xây dựng:3.452 m2</div>
<div>Diện tích xây dựng:2.021 m2</div>
<div>Mật độ xây dựng:58,6%</div>
<div>Tổng diện tích sàn:46.408 m2</div>
<div>(không kể 02 tầng hầm, kỹ thuật, kỹ thuật mái, ô kỹ thuật thang máy)</div>
<div>Tầng cao:25 tầng (Tầng 1+2 là kiot thương mại; tầng 3+4 khu văn phòng cho thuê; tầng 5 là khu dịch vụ phức hợp bao gồm: bể bơi 4 mùa 500m, GYM, Spa, giải trí,... từ tầng 6 đến 24 căn hộ chung cư cao cấp, tầng 25 là tầng kỹ thuật )</div>
<div>Chiều cao công trình:84,55m</div>
<div>Diện tích căn hộ: từ71 – 356 m2 cho nhu cầu 2PN - 4PN</div>
<div></div>
<div><span class="bold">Nguồn:<a href="http://kenhbatdongsanviet.com/tsg-lotus-sai-dong">http://kenhbatdongsanviet.com/tsg-lotus-sai-dong</a></span></div>
</div>
</div>